;;;; -*- mode: common-lisp -*-
(declaim (optimize safety))

(defpackage "DICE"
  (:use "CL")
  (:use "BLUEWOLF-UTILITIES")
  (:use "IT.BESE.FIVEAM")
  (:export "ROLL-DIE"
	   "ROLL-STANDARD-DIE"
	   "D2"
	   "D4"
	   "D6"
	   "D8"
	   "D10"
	   "D12"
	   "MAKE-DICE-VECTOR"
	   "RUN-COUNTING-SIMULATION"
	   "PRODUCE-TWO-RANDOM-NUMBERS"))
(in-package "DICE")


(def-suite dice-test-suite :description "Test DICE.")
(in-suite dice-test-suite)

;;; Fundamental dice generators
;; roll-die : natural-number positive-integer -> natural-number
(defun roll-die (n m)
  "Return a randomly selected natural number between N and M, N <= M."
  (check-type n bu:natural-number)
  (check-type m bu:positive-integer)
  (+ n (random m)))
;; Tests:
(test dice-1-6
  (is (not (find nil (loop for i from 1 to 1000 collect (<= 1 (roll-die 1 6) 6)))) t))
(test dice-10-20
  (is (not (find nil (loop for i from 1 to 1000 collect (<= 10 (roll-die 10 20) 20)))) t))
(test dice-3-3
  (is (not (find nil (loop for i from 1 to 1000 collect (<= 3 (roll-die 3 3) 3)))) t))
(test dice-20-10
  (is (not (find nil (loop for i from 1 to 1000 collect (<= 10 (roll-die 20 10) 20)))) t))

;; roll-standard-die : positive-integer -> positive-integer
(defun roll-standard-die (n)
  "Return a randomly selected integer between 1 and N."
  (check-type n bu:positive-integer)
  (roll-die 1 n))
;; Tests:
(test dice-1-1
  (is (not (find nil (loop for i from 1 to 1000 collect (<= 1 (roll-die 1 1) 1)))) t))
(test dice-1-8
  (is (not (find nil (loop for i from 1 to 1000 collect (<= 1 (roll-die 1 8) 8)))) t))
(test dice-1-100
  (is (not (find nil (loop for i from 1 to 1000 collect (<= 1 (roll-die 1 100) 100)))) t))

;;; Specialized dice
;; d2 : -> positive-integer
(defun d2 ()
  "Return a randomly selected integer between 1 and 2."
  (roll-standard-die 2))
;; Test:
(test dice-d2
  (is (not (find nil (loop for i from 1 to 1000 collect (<= 1 (d2) 2)))) t))

;; d4 : -> positive-integer
(defun d4 ()
  "Return a randomly selected integer between 1 and 4."
  (roll-standard-die 4))
;; Test:
(test dice-d4
  (is (not (find nil (loop for i from 1 to 1000 collect (<= 1 (d4) 4)))) t))

;; d6 : -> positive-integer
(defun d6 ()
  "Return a randomly selected integer between 1 and 6."
  (roll-standard-die 6))
;; Test:
(test dice-d6
  (is (not (find nil (loop for i from 1 to 1000 collect (<= 1 (d6) 6)))) t))

;; d8 : -> positive-integer
(defun d8 ()
  "Return a randomly selected integer between 1 and 8."
  (roll-standard-die 8))
;; Test:
(test dice-d8
  (is (not (find nil (loop for i from 1 to 1000 collect (<= 1 (d8) 8)))) t))

;; d10 : -> positive-integer
(defun d10 ()
  "Return a randomly selected integer between 1 and 10."
  (roll-standard-die 10))
;; Test:
(test dice-d10
  (is (not (find nil (loop for i from 1 to 1000 collect (<= 1 (d10) 10)))) t))

;; d12 : -> positive-integer
(defun d12 ()
  "Return a randomly selected integer between 1 and 12."
  (roll-standard-die 12))
;; Test:
(test dice-d12
  (is (not (find nil (loop for i from 1 to 1200 collect (<= 1 (d12) 12)))) t))

;;; Dice vectors
;; Make a vector of N S-sided dice.
;; make-dice-vector : natural-number positive-integer -> (vectorof positive-integer)
(defun make-dice-vector (n s)
  "Make a vector of N S-sided dice."
  (check-type n bu:natural-number)
  (check-type s bu:positive-integer)
  (make-array n :initial-contents (loop for i from 1 to n collect (roll-standard-die s))))
;; Tests:
(test dice-vector-3-6
  (let ((dice-vector (make-dice-vector 3 6)))
    (and (is (vectorp dice-vector) t)
	 (is (not (find nil (loop for die across dice-vector collect (<= 1 die 6)))) t))))

(test dice-vector-10-8
  (let ((dice-vector (make-dice-vector 10 8)))
    (and (is (vectorp dice-vector) t)
	 (is (not (find nil (loop for die across dice-vector collect (<= 1 die 8)))) t))))

;; sum-dice-vector : (vectorof real-number) -> real-number
(defun sum-dice-vector (vector)
  ;; (loop for x across vector summing x)
  (reduce #'+ vector))
;; Tests:
(test sum-vector-1-1-1-3
  (is (sum-dice-vector (make-array 3 :initial-contents '(1 1 1))) 3))
(test sum-vector-1-3-5-9
  (is (sum-dice-vector (make-array 3 :initial-contents '(1 3 5))) 9))

;; run-counting-simulation : natural-number positive-integer natural-number -> void
(defun run-counting-simulation (count n s)
  "Throw N S-sided dice COUNT number of times."
  (check-type count bu:natural-number)
  (check-type n bu:positive-integer)
  (check-type s bu:natural-number)
  (let* ((max-sum (* n s))
	 (counts (make-array (1+ max-sum) ; for indexing
			     :initial-element 0))
	 (dice (make-array n))
	 (sum 0))
    (dotimes (i count)
      (dotimes (j n)
	(setf (aref dice j) (roll-standard-die s)))
      (setf sum (sum-dice-vector dice))
      (incf (aref counts sum)))
    (loop for i from n to max-sum
       do (format t "~%~3a : ~a" i (aref counts i)))))

;; produce-two-random-numbers : positive-integer -> positive-integer positive-integer
(defun produce-two-random-numbers (n)
  "Produce two different random numbers between 1 and N."
  (check-type n bu:positive-integer)
  (do ((l (roll-standard-die n) (roll-standard-die n))
       (m (roll-standard-die n) (roll-standard-die n)))
      ((not (eql l m)) (values l m))))
;; Tests:
(test distinct-random-numbers
  (dotimes (i 10000)
    (multiple-value-bind (a b) (produce-two-random-numbers 10)
      (is-false (eql a b)))))
