(defsystem :dice
  ;; :default-component-class cl-source-file.cl
  :description "Throw dice."
  :version "0.0"
  :author "Udyant Wig <udyant.wig@gmail.com>"
  :licence "Undecided"
  :depends-on ("fiveam" "bluewolf-utilities")
  :components ((:file "dice")))
